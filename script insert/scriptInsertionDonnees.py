import pandas as pd
import psycopg2 as psy

client = pd.read_csv(r'Client.csv')
df = pd.DataFrame(client)
df2 = df.drop_duplicates()
df3 = df2.dropna()
print(df3)

co = None
try:
		co = psy.connect(host='londres', # Changer les variables host, database, user et password par vos propres identifiants afin de générer les lignes dans les tables.
						database='dbletuaillon',
						user='letuaillon',
						password='*********')
	
		curs=co.cursor()
		

		for row in df3.itertuples():
			curs.execute('''INSERT INTO CLIENT VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);''',
			(row.idClient, row.nom, row.prenom, row.date_naissance, row.adresse, row.ville, row.pays, row.code_postal, row.telephone, row.email, row.type,row.reduc))

		categorie = pd.read_csv(r'Categorie.csv')
		df = pd.DataFrame(categorie)
		df2 = df.drop_duplicates()
		df3 = df2.dropna()
		print(df3)
  
		for row in df3.itertuples():
			curs.execute('''INSERT INTO CATEGORIE VALUES (%s,%s);''',
			(row.idCategorie, row.nom))
   
		fournisseur = pd.read_csv(r'Fournisseur.csv')
		df = pd.DataFrame(fournisseur)
		df2 = df.drop_duplicates()
		df3 = df2.dropna()
		print(df3)
  
		for row in df3.itertuples():
			curs.execute('''INSERT INTO FOURNISSEUR VALUES (%s,%s,%s,%s,%s,%s,%s);''',
			(row.idFournisseur,row.nom,row.adresse,row.ville,row.code_postal,row.telephone,row.email))

		produit = pd.read_csv(r'produit.csv')
		df = pd.DataFrame(produit)
		df2 = df.drop_duplicates()
		df3 = df2.dropna()
		print(df3)
  
		for row in df3.itertuples():
			curs.execute('''INSERT INTO PRODUIT VALUES (%s,%s,%s,%s,%s,%s);''',
			(row.idproduit,row.nom,row.marque,row.taille,row.prix,row.idcategorie))

		magasin = pd.read_csv(r'Magasin.csv')
		df = pd.DataFrame(magasin)
		df2 = df.drop_duplicates()
		df3 = df2.dropna()
		print(df3)
  
		for row in df3.itertuples():
			curs.execute('''INSERT INTO MAGASIN VALUES (%s,%s,%s,%s,%s,%s,%s);''',
			(row.idmagasin,row.nom,row.adresse,row.ville,row.code_postal,row.telephone,row.email))

		reserver = pd.read_csv(r'Reserver.csv')
		df = pd.DataFrame(reserver)
		df2 = df.drop_duplicates()
		df3 = df2.dropna()
		print(df3)
  
		for row in df3.itertuples():
			curs.execute('''INSERT INTO RESERVER VALUES (%s,%s,%s,%s,%s);''',
			(row.idclient,row.idproduit,row.quantite,row.date_debut,row.date_fin))

		stocker = pd.read_csv(r'stocker.csv')
		df = pd.DataFrame(stocker)
		df2 = df.drop_duplicates()
		df3 = df2.dropna()
		print(df3)
  
		for row in df3.itertuples():
			curs.execute('''INSERT INTO STOCKER VALUES (%s,%s,%s);''',
			(row.idmagasin,row.idproduit,row.quantite))

		appro = pd.read_csv(r'Appro.csv')
		df = pd.DataFrame(appro)
		df2 = df.drop_duplicates()
		df3 = df2.dropna()
		print(df3)
  
		for row in df3.itertuples():
			curs.execute('''INSERT INTO APPROVISIONNER VALUES (%s,%s,%s,%s);''',
			(row.idfournisseur,row.idproduit,row.quantite,row.date_approvisionement))


		co.commit()
		curs.close()

except (Exception , psy. DatabaseError ) as error :
		print (error)
	
finally:
		if co is not None:
			co.close()