--Question 1
SELECT *
FROM Produit
WHERE idProduit NOT IN (SELECT idProduit FROM RESERVER);
-- Cette requête sélectionne les noms des produits dont l'identifiant n'apparaît pas dans la table Location, 
--ce qui signifie qu'ils n'ont jamais été loués.


--Question 2 :
SELECT c.type, SUM((p.prix)*(CURRENT_DATE-r.date_debut)) AS prixSansReduction,  SUM((p.prix*c.reduction)*(CURRENT_DATE-r.date_debut)) AS prix_Avec_Reduction
-- Sélectionner les tables client produit et reserver
FROM Client c, Produit p, RESERVER r
WHERE c.type='entreprise' and c.idclient = r.idclient and r.idproduit = p.idproduit and (r.date_debut <= (CURRENT_DATE) and r.date_debut >= (CURRENT_DATE-7))
-- Selectionne les reservations des clients de type entreprise qui sont datés d'il y a moins de 7 jours (semaine dernière avec réduction) et qui ne depasse pas la date du jour.
GROUP BY c.type;

--Question 3
SELECT *
FROM Produit
WHERE idProduit NOT IN (SELECT idProduit FROM RESERVER);
--Même question que la 1 formulé autrement. Donc même requête.


--Question 4
-- Calculer le montant des locations en fonction des types de clients et du nombre de jours loues
SELECT c.type, SUM((p.prix*c.reduction)*(CURRENT_DATE-r.date_debut)) AS Total 
-- Sélectionner les tables client produit et reserver
FROM Client c, Produit p, RESERVER r
WHERE c.idclient = r.idclient and r.idproduit = p.idproduit and (r.date_debut <= (CURRENT_DATE) and r.date_debut >= (CURRENT_DATE-30))  --Le to_date ne fonctionnant pas dans la base de l'iut nous avons décider de faire CURRENT_DATE-30 pour prendre l'intervale des reservations datés d'il y a moins d'un mois.
-- Selectionne les reservations qui sont datÉs d'il y a moins de 30 jours et qui ne depasse pas la date du jour.
GROUP BY c.type;
-- Regrouper les résultats par type de client
