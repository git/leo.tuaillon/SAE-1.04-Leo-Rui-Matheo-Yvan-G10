DROP TABLE IF EXISTS APPROVISIONNER;
DROP TABLE IF EXISTS STOCKER;
DROP TABLE IF EXISTS RESERVER;
DROP TABLE IF EXISTS PRODUIT;
DROP TABLE IF EXISTS MAGASIN;
DROP TABLE IF EXISTS FOURNISSEUR;
DROP TABLE IF EXISTS CLIENT;
DROP TABLE IF EXISTS CATEGORIE;

CREATE TABLE CATEGORIE (
  idcategorie char(5) NOT NULL UNIQUE,
  nom varchar(30) NOT NULL,
  PRIMARY KEY (idcategorie)
);

CREATE TABLE CLIENT (
  idclient char(5) NOT NULL UNIQUE,
  nom varchar(30) NOT NULL,
  prenom varchar(20) NOT NULL,
  date_de_naissance date,
  adresse varchar(42),
  ville varchar(30),
  pays char(2),
  code_postal char(5),
  telephone char(10),
  email varchar(40) NOT NULL,
  type varchar CHECK(type IN ('particulier','entreprise','ecole','centre de loisir')),
  reduction numeric NOT NULL,
  PRIMARY KEY (idclient)
);

CREATE TABLE FOURNISSEUR (
  idfournisseur char(5) NOT NULL UNIQUE,
  nom varchar(30) NOT NULL,
  adresse varchar(40) NOT NULL,
  ville varchar(30) NOT NULL,
  code_postal char(5) NOT NULL,
  telephone char(10) NOT NULL,
  email varchar(40) NOT NULL,
  PRIMARY KEY (idfournisseur)
);

CREATE TABLE MAGASIN (
  idmagasin char(5) NOT NULL UNIQUE,
  nom varchar(30) NOT NULL,
  adresse varchar(40) NOT NULL,
  ville varchar(30) NOT NULL,
  code_postal char(5) NOT NULL,
  telephone char(10) NOT NULL,
  email varchar(42) NOT NULL,
  PRIMARY KEY (idmagasin)
);

CREATE TABLE PRODUIT (
  idproduit char(5) NOT NULL UNIQUE,
  nom varchar(40) NOT NULL,
  marque varchar(30) NOT NULL,
  taille varchar(20) NOT NULL,
  prix numeric NOT NULL,
  idcategorie char(5) NOT NULL,
  FOREIGN KEY (idcategorie) REFERENCES CATEGORIE (idcategorie),
  PRIMARY KEY (idproduit)
);

CREATE TABLE RESERVER (
  idclient char(5) NOT NULL,
  idproduit char(5) NOT NULL,
  quantite numeric NOT NULL,
  date_debut date NOT NULL,
  date_fin date NOT NULL,
  FOREIGN KEY (idproduit) REFERENCES PRODUIT (idproduit),
  FOREIGN KEY (idclient) REFERENCES CLIENT (idclient),
  PRIMARY KEY (idclient, idproduit)
);

CREATE TABLE STOCKER (
  idmagasin char(5) NOT NULL,
  idproduit char(5) NOT NULL,
  quantite numeric NOT NULL,
  FOREIGN KEY (idproduit) REFERENCES PRODUIT (idproduit),
  FOREIGN KEY (idmagasin) REFERENCES MAGASIN (idmagasin),
  PRIMARY KEY (idmagasin, idproduit)
);

CREATE TABLE APPROVISIONNER (
  idfournisseur char(5) NOT NULL,
  idproduit char(5) NOT NULL,
  quantite numeric NOT NULL,
  date_approvisionnement date,
  FOREIGN KEY (idproduit) REFERENCES PRODUIT (idproduit),
  FOREIGN KEY (idfournisseur) REFERENCES FOURNISSEUR (idfournisseur),
  PRIMARY KEY (idfournisseur, idproduit)
);
