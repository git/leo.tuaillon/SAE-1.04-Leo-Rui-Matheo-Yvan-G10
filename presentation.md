# Présentation entreprise : Skiklö
```Elèves```  : MARTINS DA SILVA GOMES Rui / CALATAYUD Yvan / TUAILLON Léo / HERSAN Mathéo  
                 
Groupe : 10

Le directeur de ```Skiklö```, qui désire informatiser la gestion de son établissement, nous a chargé de modéliser le système d'information.
L'entreprise ```Skiklö``` est une entreprise de location d'équipement de sports d'hiver. Elle est implantée aux stations des Deux Alpes, Courchevel et Villard de Lans. 
Chaque client est identifié par un numéro unique à x chiffres qui lui est attribué lorsqu'il réserve ou loue un produit.
A l'arrivée d'un nouveau client nous enregistrons ses informations personnelles afin de le retrouver en cas de vol ou de casse.
On connaît son nom, prénom, date de naissance, adresse, ville, pays, Code Postal, Téléphone, son email et aussi son type c'est à dire si c'est un particulier ou une entreprise.

Chaque type de produit est identifié par un numéro unique, un nom, une marque, une taille et un prix par jour. Les équipements de sports d'hiver que nous louons dans nos boutiques respectives sont classés dans différentes catégories identifié un numéro unique et un nom.(ski de piste, ski de fond, luge, protections...).

Tous les produits que nous recevons sont enregistrés afin que nous puissions connaître la quantité, la date d'approvisionnement et les informations concernant le fournisseur tel que son nom, son adresse, ses informations de contact. Les produits sont ensuite stockés dans nos magasins, ceux-ci sont repartis dans différentes villes de France, chaque magasins est doté d'un nom, d'une adresse et d'un numéro de téléphone pour pouvoir contacter le gérant en cas, par exemple, de rupture de stocks.






## MCD et MLD de Skiklö
MCD :  

![image](https://codefirst.iut.uca.fr/git/matheo.hersan/Tout_et_rien/raw/branch/master/SAE1.04/MCD-MLD-FINAL/MCD.png)

Nous avons décidé de mettre l'attribut quantité dans la relation stocker et à la base nous pensions rajouter un attribut quantité totale dans la table Produit. Mais dans le cadre de notre entreprise la quantité totale ne sera utile que dans le cadre d'un inventaire, ce qui nous arrive qu'une seule fois par an. Nous avons donc pas jugé utile de rajouter un élément dans la table produit puisque en faisant une requête qui additionne la quantité de chacun de nos magasins pour un produit donné nous aurions le même résultat. Certe la requête est plus complexe mais le magasin ne la fera pas souvent (une fois par ans), donc celà permet de ne pas surcharger inutilement la table Produits de données. 
        
Au départ nous avions pensé lister chaque produit dans la table produit avec un attribut "Statut" pour chaque produit qui contiendrait la valeur char "Réservé, Louée ou bien disponible". Nous nous sommes assez vite rendu compte que ce n'était pas très optimisé car si nous avons 150 fois la même paire de ski par exemple, cela impliquerait 150 lignes identiques dans la table produit à l'exception de l'idProduit qui changerait. Nous avons donc décidé de lister dans la table produit une référence de produit par ligne, puis de mettre une relation 'stocker' entre les produits et les magasins afin de savoir combien de produit d'un type donné est stocké dans quel magasin. Cela permet que, lorsqu'on éffectue une réservation pour un client, on peut vérifier pour une date donnée combien de produits sont disponible ou non.
MLD :  

![image](https://codefirst.iut.uca.fr/git/matheo.hersan/Tout_et_rien/raw/branch/master/SAE1.04/MCD-MLD-FINAL/MLD.png)

##